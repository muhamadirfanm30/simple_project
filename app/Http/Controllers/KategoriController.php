<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function index()
    {
        return view('kategori.index', [
            'title' => 'List Kategori',
            'list_kategory' => Kategori::get(),
        ]);
    }

    public function create()
    {
        return view('kategori.create', [
            'title' => 'List Kategori'
        ]);
    }

    public function update($id)
    {
        return view('kategori.update', [
            'title' => 'Edit Kategori',
            'data' => Kategori::find($id),
        ]);
    }

    public function store(Request $request)
    {
        $save = Kategori::create([
            'no_kategori' => $this->generateOrderCode(),
            'nama_kategori' => $request->nama_kategori,
            'keterangan' => $request->keterangan,
        ]);

        return redirect()->route('kategori.index')->with('success', 'oke');
    }

    public function edit(Request $request, $id)
    {
        $save = Kategori::where('id', $id)->update([
            'nama_kategori' => $request->nama_kategori,
            'keterangan' => $request->keterangan,
        ]);

        return redirect()->route('kategori.index')->with('success', 'oke');
    }

    public function delete($id)
    {
        $save = Kategori::where('id', $id)->delete();

        return redirect()->back('kategori.index')->with('success', 'oke');
    }

    public function generateOrderCode()
    {
        $q          = Kategori::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmY');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = 'K-' .  ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Kategori::orderBy('id', 'desc')->value('no_kategori');
            $last     = explode("-", $last);
            $order_code = 'K-' .  (sprintf("%04s", $last[1] + 1));
        }
        return $order_code;
    }


}
