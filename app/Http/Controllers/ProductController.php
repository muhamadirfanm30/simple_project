<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\LogActivity;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index()
    {
        return view('product.index', [
            'title' => "List Product",
            'list_product' => Product::get()
        ]);
    }

    public function create()
    {
        return view('product.create', [
            'title' => "Create Product",
            'get_category' => Kategori::get()
        ]);
    }

    public function update($id)
    {
        // return Product::where('id', $id)->first();
        return view('product.update', [
            'title' => "Update Product",
            'get_category' => Kategori::get(),
            'dataEdit' => Product::where('id', $id)->first()
        ]);
    }

    public function store(Request $request)
    {
        $save = Product::create([
            'kd_barang' => $this->generateOrderCode(),
            'no_kategory' => $request->no_kategory,
            'nama_barang' => $request->nama_barang,
            'harga_barang' => $request->harga_barang,
            'stock_ahir' => $request->stock_ahir,
            'keterangan' => $request->keterangan,
        ]);

        $newData = Product::with('kategori')->where('id', $save->id)->first();

        LogActivity::create([
            'no_transaksi' => $newData->kd_barang,
            'playload_after' => json_encode($newData),
            'user_id' => Auth::id(),
            'log_type' => 'create',
            'log_modul' => 'Product'
        ]);

        return redirect()->route('product.index')->with('success', 'oke');
    }

    public function edit($id, Request $request)
    {
        $save = Product::where('id', $id)->update([
            'kd_barang' => $this->generateOrderCode(),
            'no_kategory' => $request->no_kategory,
            'nama_barang' => $request->nama_barang,
            'harga_barang' => $request->harga_barang,
            'stock_ahir' => $request->stock_ahir,
            'keterangan' => $request->keterangan,
        ]);

        $newData = Product::with('kategori')->where('id', $id)->first();

        LogActivity::create([
            'no_transaksi' => $newData->kd_barang,
            'playload_after' => json_encode($newData),
            'user_id' => Auth::id(),
            'log_type' => 'Update',
            'log_modul' => 'Product'
        ]);

        return redirect()->route('product.index')->with('success', 'oke');
    }

    public function destroy($id, Request $request)
    {
        $save = Product::where('id', $id)->delete();

        return redirect()->route('product.index')->with('success', 'oke');
    }

    public function generateOrderCode()
    {
        $q          = Product::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmY');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = 'B-' .  ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Product::orderBy('id', 'desc')->value('kd_barang');
            $last     = explode("-", $last);
            $order_code = 'B-' .  (sprintf("%04s", $last[1] + 1));
        }
        return $order_code;
    }
}
