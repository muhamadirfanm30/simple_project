<?php

namespace App\Http\Controllers;

use App\Models\LogActivity;
use Illuminate\Http\Request;

class LogActivityController extends Controller
{
    public function index()
    {
        return view('log.index', [
            'title' => 'List Activity',
            'list_log' => LogActivity::get()
        ]);
    }
}
