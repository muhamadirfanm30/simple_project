<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use DB;

class LaporanController extends Controller
{
    public function index()
    {
        $transaksi = Transaksi::select(DB::raw('kategoris.id, kategoris.nama_kategori, COUNT(*) AS count_data'), DB::raw('SUM(transaksis.total_harga) As revenue'))
        ->join('kategoris', 'kategoris.id', '=', 'transaksis.kategori_id')
        ->groupBy('kategoris.nama_kategori')->get();

        // cart
        $label = [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember",
        ];

        for ($bulan = 1; $bulan < 13; $bulan++) {
            $elektronik = collect(DB::SELECT("SELECT sum(total_harga) AS jumlah from transaksis where month(tgl_transaksi)='$bulan'"))->first();
            $fashion = collect(DB::SELECT("SELECT sum(total_harga) AS jumlah from transaksis where month(created_at)='$bulan'"))->first();
            $rumah_tangga = collect(DB::SELECT("SELECT avg(total_harga) AS jumlah from transaksis where month(created_at)='$bulan'"))->first();
            $data_rumah_tangga[] = $rumah_tangga->jumlah;
            $data_fashion[] = $fashion->jumlah;
            $data_elektronik[] = $elektronik->jumlah;
        }
        
        return view('laporan.index', [
            'title' => "Laporan",
            // 'kategori' => $kategori,
            // 'trans' => $trans,
            'data_elektronik' => $data_elektronik,
            'elektronik' => $elektronik,
            'data_fashion' => $data_fashion,
            'fashion' => $fashion,
            'data_rumah_tangga' => $data_rumah_tangga,
            'rumah_tangga' => $rumah_tangga,
            'list_laporan' => $transaksi,
            'label' => $label,
        ]);
    }
}
