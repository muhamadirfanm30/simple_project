<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Product;
use App\Models\Transaksi;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    public function index()
    {
        // return Transaksi::with(['produk', 'kategori'])->get();
        return view('transaksi.index', [
            'title' => 'List Transaksi',
            'list_transaksi' => Transaksi::with(['produk', 'kategori'])->get()
        ]);
    }

    public function create()
    {
        return view('transaksi.create', [
            'title' => 'Create Transaksi',
            'get_category' => Kategori::get(),
            'get_prod' => Product::with('kategori')->get()
        ]);
    }

    public function update($id)
    {
        return view('transaksi.update', [
            'title' => 'Update Transaksi',
            'data_edit' => Transaksi::find($id),
            'get_category' => Kategori::get(),
            'get_prod' => Product::with('kategori')->get()
        ]);
    }

    public function getProduk($id)
    {
        $selectProd = Product::where('id', $id)->first();
        return $selectProd;
    }

    public function select2($id)
    {
        $getSelect = Product::where('no_kategory', $id)->get();
        return $getSelect;
    }

    public function store(Request $request)
    {
        $saveTransaksi = Transaksi::create([
            'kd_transaksi' => $this->generateOrderCode(),
            'tgl_transaksi' => $request->tgl_transaksi,
            'kd_barang' => $request->kd_barang,
            'jml_barang' => $request->jml_barang,
            'harga_satuan' => $request->harga_satuan,
            'total_harga' => $request->total_harga,
            'kategori_id' => $request->kategori_id,
        ]);

        return redirect()->route('transaksi.index')->with('success', 'ok');
    }

    public function edit(Request $request, $id)
    {
        $saveTransaksi = Transaksi::find($id)->update([
            'tgl_transaksi' => $request->tgl_transaksi,
            'kd_barang' => $request->kd_barang,
            'jml_barang' => $request->jml_barang,
            'harga_satuan' => $request->harga_satuan,
            'total_harga' => $request->total_harga,
            'kategori_id' => $request->kategori_id,
        ]);

        return redirect()->route('transaksi.index')->with('success', 'ok');
    }

    public function generateOrderCode()
    {
        $q          = Transaksi::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmY');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = 'TRANS-' .  ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Transaksi::orderBy('id', 'desc')->value('kd_transaksi');
            $last     = explode("-", $last);
            $order_code = 'TRANS-' .  (sprintf("%04s", $last[1] + 1));
        }
        return $order_code;
    }
}
