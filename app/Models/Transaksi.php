<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = "transaksis";
    protected $fillable = [
        'kd_transaksi',
        'tgl_transaksi',
        'kd_barang',
        'jml_barang',
        'harga_satuan',
        'total_harga',
        'kategori_id',
    ];

    protected $appends = [
        'count_fashion',
    ];

    public function getCountFashionAttribute()
    {
        // $query = Transaksi::whereHas('kategori', function ($q) {
        //     $q->where('id', $this->kategori_id);
        // })->whereDay('tgl_transaksi', '05');
        // // if (Session::get('date_range')) {
        // //     $query = $query->dateRange(Session::get('date_range'));
        // // }
        // if ($query) {
        //     return $query = $query->sum('total_harga');
        // }
        // return null;
        $getCategory = Kategori::where('id', $this->kategori_id)->first();
        return Transaksi::where('kategori_id',  $this->kategori_id)->count();
    }

    /**
     * Get the user that owns the Transaksi
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function produk()
    {
        return $this->belongsTo(Product::class, 'kd_barang', 'id');
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id', 'id');
    }
}
