<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    use HasFactory;
    protected $table = "log_activities";
    protected $fillable = [
        'no_transaksi',
        'playload_after',
        'user_id',
        'log_type',
        'log_modul',
    ];
}
