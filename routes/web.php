<?php

use App\Http\Controllers\KategoriController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\LogActivityController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransaksiController;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $list_product = Product::get();
    return view('landingpage', [
        'list_product' => $list_product 
    ]);
    
    // return view('auth.login');
});

Route::get('/login', function () {
    return view('auth.login');
});
Route::get('/cv', function () {
    return view('cv');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/logout-users', [App\Http\Controllers\HomeController::class, 'logoutUser'])->name('logout-users');

Route::middleware('auth')->group(function () {
    Route::prefix('admin')->group(function () {
        //product

        Route::prefix('product')->group(function () {
            Route::get('/', [ProductController::class, 'index'])->name('product.index');
            Route::get('/create', [ProductController::class, 'create'])->name('product.create');
            Route::get('/update/{id}', [ProductController::class, 'update'])->name('product.update');
            Route::post('/store', [ProductController::class, 'store'])->name('product.store');
            Route::post('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
            Route::post('/destroy', [ProductController::class, 'destroy'])->name('product.destroy');
        });

        Route::prefix('kategori')->group(function () {
            Route::get('/', [KategoriController::class, 'index'])->name('kategori.index');
            Route::get('/create', [KategoriController::class, 'create'])->name('kategori.create');
            Route::get('/update/{id}', [KategoriController::class, 'update'])->name('kategori..update');
            Route::post('/store', [KategoriController::class, 'store'])->name('kategori.store');
            Route::post('/edit/{id}', [KategoriController::class, 'edit'])->name('kategori.edit');
            Route::post('/delete/{id}', [KategoriController::class, 'delete'])->name('kategori.delete');
        });

        Route::prefix('log-activity')->group(function () {
            Route::get('/', [LogActivityController::class, 'index'])->name('transaksi.index');
        });
        
    });
});
