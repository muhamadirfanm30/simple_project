@extends('home')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/transaction') }}" class="btn btn-primary add-banners"> Kembali</a><br>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/transaction/edit/'.$data_edit->id) }}" method="post">
            @csrf
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Tgl Transaksi</label>
                    <input type="date" name="tgl_transaksi" value="{{ $data_edit->tgl_transaksi }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Kategory</label>
                    <select name="kategori_id" class="form-control" id="">
                        <option value="">Select ....</option>
                        @foreach($get_category as $cat)
                            <option value="{{ $cat->id }}" {{ $cat->id == $data_edit->kategori_id ? 'selected' : '' }}>{{ $cat->nama_kategori }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Nama Barang</label>
                    <select name="kd_barang" class="form-control get_product_id" id="">
                        <option value="">Select ....</option>
                        @foreach($get_prod as $prod)
                            <option value="{{ $prod->id }}" {{ $prod->id == $data_edit->kd_barang ? 'selected' : '' }}>{{ $prod->nama_barang }} - ({{ $prod->kategori->nama_kategori }})</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Jumlah Barang</label>
                    <input type="text" name="jml_barang" onchange="total()" value="{{ $data_edit->jml_barang }}" class="form-control get_jumlah_barang" required>
                </div>
                <div class="form-group">
                    <label for="">Harga Satuan</label>
                    <input type="text" name="harga_satuan" value="{{ $data_edit->harga_satuan }}" class="form-control harga_satuan_barang" readonly required>
                </div>
                <div class="form-group">
                    <label for="">Total Harga</label>
                    <input type="text" name="total_harga" value="{{ $data_edit->total_harga }}" class="form-control get_total" readonly required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Simpan</button>
                </div>
        
            </div>
        </form>
    </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
    $('.get_product_id').on('change', function() {
        $('.get_jumlah_barang').val('')
        $('.get_total').val('')
        alert( this.value );
        $.ajax({
            url:HelperService.apiUrl('/get-data-produk/' + this.value),
            type: 'GET',
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response.nama_barang)
                $('.harga_satuan_barang').val(response.harga_barang)
            },
            error: function(xhr, status, error) {
                alert('error')
            },
        });
    });

    function total() 
    {
        var jml = $(".get_jumlah_barang").val()*1 ;
        var harga_satuan= $(".harga_satuan_barang").val();
        console.log('jumlah = ' + jml)
        console.log('harga satuan = ' + harga_satuan)
        var total = jml * harga_satuan;
        $(".get_total").val(total) ;
    }
    // $(document).on('change', '.get_product_id', function() {
    //     campsite_id = $("option:selected", this);
    //     loadSelectProd(campsite_id);
    // })

    // function loadSelectProd(campsite_id) {
    //     HelperService
    //         .select2Static(".product_id", '/api/load-select2-product/'+campsite_id, function(item) {
    //             console.log(item)
    //             return {
    //                 id: item.id,
    //                 text: item.id,
    //                 data: item,
    //             }
    //         })
    // }
</script>
@endsection