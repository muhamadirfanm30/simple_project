@extends('home')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/transaction/create') }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> TAMBAH TRANSAKSI</a><br>
        </div>
    </div>
    <div class="card-body">
        
        <table id="example" class="table table-striped table-bordered">
            <thead>
                <th>Kode Transaksi</th>
                <th>tgl Transaksi</th>
                <th>Kode barang</th>
                <th>Nama Barang</th>
                <th>Kategori</th>
                <th>Harga Satuan</th>
                <th>Jumlah</th>
                <th>Total</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach($list_transaksi as $trans)
                    <tr>
                        <td>{{ $trans->kd_transaksi }}</td>
                        <td>{{ date('M D Y', strtotime($trans->tgl_transaksi)) }}</td>
                        <td>{{ $trans->produk->kd_barang }}</td>
                        <td>{{ $trans->produk->nama_barang }}</td>
                        <td>{{ $trans->kategori->nama_kategori }}</td>
                        <td>Rp. {{ number_format($trans->produk->harga_barang) }}</td>
                        <td>{{ $trans->jml_barang }}</td>
                        <td>Rp. {{ number_format($trans->total_harga) }}</td>
                        <td>
                            <a href="{{ url('admin/transaction/update/'.$trans->id) }}" class="btn btn-success btn-sm">Edit</a>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection