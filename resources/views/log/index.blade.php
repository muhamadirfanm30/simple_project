@extends('home')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <!-- <a href="{{ url('/admin/product/create') }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> TAMBAH PRODUK</a><br> -->
        </div>
    </div>
    <div class="card-body">
        
        <table id="example" class="table table-striped table-bordered">
            <thead>
                <th>Code</th>
                <th>User</th>
                <th>Type</th>
                <th>Module</th>
                <th>Date</th>
            </thead>
            <tbody>
                @foreach($list_log as $list)
                    <tr>
                        <td>{{ $list->no_transaksi }}</td>
                        <td>{{ $list->user_id }}</td>
                        <td>{{ $list->log_type }}</td>
                        <td>{{ $list->log_modul }}</td>
                        <td>{{ $list->created_at }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function() {
        $('#example').DataTable();  
    } );
</script>
@endsection