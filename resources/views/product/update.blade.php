@extends('home')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/product') }}" class="btn btn-primary add-banners"> Kembali</a><br>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/product/edit/'.$dataEdit->id) }}" method="post">
            @csrf
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Kategory</label>
                    <select name="no_kategory" class="form-control" id="">
                        <option value="">Select ....</option>
                        @foreach($get_category as $cat)
                            <option value="{{ $cat->id }}" {{ $cat->id == $dataEdit->no_kategory ? 'selected' : false }}>{{ $cat->no_kategori }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Nama Barang</label>
                    <input type="text" name="nama_barang" class="form-control" value="{{ $dataEdit->nama_barang }}" placeholder="Nama Barang">
                </div>
                <div class="form-group">
                    <label for="">Harga Barang</label>
                    <input type="number" name="harga_barang" value="{{ $dataEdit->harga_barang }}" class="form-control" placeholder="Harga Barang">
                </div>
                <div class="form-group">
                    <label for="">Stok</label>
                    <input type="text" name="stock_ahir" value="{{ $dataEdit->stock_ahir }}" class="form-control" placeholder="Stok">
                </div>

                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" class="form-control" id="" cols="30" rows="5">{{ $dataEdit->keterangan }}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')

@endsection