@extends('home')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/product/create') }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> TAMBAH PRODUK</a><br>
        </div>
    </div>
    <div class="card-body">
        
        <table id="example" class="table table-striped table-bordered">
            <thead>
                <th>Kode Barang</th>
                <th>No Kategori</th>
                <th>Nama Barang</th>
                <th>Harga Barang</th>
                <th>Stok Ahir</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach($list_product as $list)
                    <tr>
                        <td>{{ $list->kd_barang }}</td>
                        <td>{{ $list->no_kategory }}</td>
                        <td>{{ $list->nama_barang }}</td>
                        <td>{{ $list->harga_barang }}</td>
                        <td>{{ $list->stock_ahir }}</td>
                        <td>{{ $list->keterangan }}</td>
                        <td>
                            <a href="{{ url('admin/product/update/'.$list->id) }}" class="btn btn-success btn-sm">Update</a>
                            @if(Auth::user()->akses == 1)
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection