@extends('home')
@section('content')
<div class="card">
    
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/kategori') }}" class="btn btn-primary add-banners"> Kembali</a><br>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/kategori/store') }}" method="post">
            @csrf
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Nama Kategori</label>
                    <input type="text" name="nama_kategori" class="form-control" placeholder="Nama Kategori">
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" class="form-control" id="" cols="30" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')

@endsection