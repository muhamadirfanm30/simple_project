@extends('home')
@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        <a href="{{ url('/admin/kategori/create') }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> TAMBAH KATEGORI</a><br>
        </div>
    </div>
    <div class="card-body">
        
        <table id="example" class="table table-striped table-bordered">
            <thead>
                <th>No Kategori</th>
                <th>Nama Kategori</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach($list_kategory as $list)
                    <tr>
                        <td>{{ $list->no_kategori }}</td>
                        <td>{{ $list->nama_kategori }}</td>
                        <td>
                            <a href="{{ url('admin/kategori/update/'.$list->id) }}" class="btn btn-success btn-sm">Update</a>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection