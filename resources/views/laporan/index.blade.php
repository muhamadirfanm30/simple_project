@extends('home')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
        </div>
    </div>
    <div class="card-body">
        
        <table id="example" class="table table-striped table-bordered">
            <thead>
                <th>Kategori</th>
                <th>Jumlah</th>
                <th>Total</th>
            </thead>
            <tbody>
                @foreach($list_laporan as $data)
                    <tr>
                        <td>{{ $data->nama_kategori }}</td>
                        <td>{{ $data->count_data }}</td>
                        <td>Rp. {{ number_format($data->revenue) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table><br><br>

        <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapseCustomer1">
                            Sales Chart
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCustomer1" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="text-center">
                                            <!-- <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong> -->
                                        </p>

                                        <div class="chart">
                                            <canvas id="mataChart" class="chartjs" width="undefined" height="undefined"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                    <div class="col-sm-4 col-4">
                                        <div class="description-block border-right">
                                        <!-- <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 17%</span> -->
                                        <h5 class="description-header">Rp. {{ number_format($elektronik->jumlah) }}</h5>
                                        <span class="description-text">TOTAL INCOME</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 col-4">
                                        <div class="description-block border-right">
                                        <!-- <span class="description-percentage text-warning"><i class="fas fa-caret-left"></i> 0%</span> -->
                                        <h5 class="description-header">Rp. {{ number_format($fashion->jumlah) }}</h5>
                                        <span class="description-text">TOTAL FINANCING</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 col-4">
                                        <div class="description-block border-right">
                                        <!-- <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 20%</span> -->
                                        <h5 class="description-header">Rp. {{ number_format($rumah_tangga->jumlah) }}</h5>
                                        <span class="description-text">TOTAL AVERAGE INCOME</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <!-- <div class="col-sm-4 col-6">
                                        <div class="description-block">
                                        <span class="description-percentage text-danger"><i class="fas fa-caret-down"></i> 18%</span>
                                        <h5 class="description-header">1200</h5>
                                        <span class="description-text">GOAL COMPLETIONS</span>
                                        </div>
                                    </div> -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<!-- ChartJS -->
<script src="{{ asset('adminAssets/chart.js/Chart.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
<script type="text/javascript">
    var ctx = document.getElementById("mataChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: <?php echo json_encode($label); ?>,
            datasets: [
                {
                    label: 'Total Income',
                    backgroundColor: '#6c5ce7',
                    borderColor: '#6c5ce7',
                    data: <?php echo json_encode($data_elektronik); ?>
                },
                {
                    label: 'Total Financing',
                    backgroundColor: '#a29bfe',
                    borderColor: '#a29bfe',
                    data: <?php echo json_encode($data_fashion); ?>
                },
                {
                    label: 'Total Average Income',
                    backgroundColor: '#74b9ff',
                    borderColor: '#74b9ff',
                    data: <?php echo json_encode($data_rumah_tangga); ?>
                },
            ],
            options: {
                animation: {
                    onProgress: function(animation) {
                        progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
                    }
                }
            }
        },
    });
</script>
@endsection

