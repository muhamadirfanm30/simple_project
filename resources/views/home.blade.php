<!DOCTYPE html>
<html lang="en">
    @include('layouts.header')
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
              <i class="fas fa-th-large"></i>
            </a>
          </li>
        </ul>
      </nav>

      <div class="content-wrapper"><br>
          <!-- Content Header (Page header) -->
          <div class="container-fluid">
            @yield('content')
          </div>
          <!-- /.End content-header -->
      </div>

      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <a href="index3.html" class="brand-link">
          <!-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
          <span class="brand-text font-weight-light">My Apps</span>
        </a>

        <div class="sidebar">
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img src="{{ asset('assets/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
          </div>

          <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
              <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-sidebar">
                  <i class="fas fa-search fa-fw"></i>
                </button>
              </div>
            </div>
          </div>

          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li class="nav-item">
                <a href="{{ url('admin/product') }}" class="nav-link">
                  <i class="nav-icon far fa-image"></i>
                  <p>
                    Product
                  </p>
                </a>
              </li>
              @if(Auth::user()->akses == 1)
              <li class="nav-item">
                <a href="{{ url('admin/log-activity') }}" class="nav-link">
                  <i class="nav-icon far fa-image"></i>
                  <p>
                    Log Product
                  </p>
                </a>
              </li>
              @endif
              <li class="nav-item">
                <a href="{{ url('logout-users') }}" onclick="confirm('Are you sure to logout?')" class="nav-link">
                  <i class="nav-icon far fa-image"></i>
                  <p>
                    Log Out
                  </p>
                </a>
              </li>
            </ul>
          </nav>
          
        </div>
        
      </aside>

      <footer class="main-footer">
        <strong>Copyright &copy; 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
          <b>Version</b> 3.1.0-rc
        </div>
      </footer>

      <aside class="control-sidebar control-sidebar-dark">
      </aside>
  </div>

    @include('layouts.script')
</body>
</html>
