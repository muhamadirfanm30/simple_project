<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>List Product</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary static-top">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9">
                        <a class="navbar-brand" href="#">
                            <img src="" alt="..." height="36">
                        </a>
                    </div>
                    <div class="col-md-3">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="{{ url('/login') }}">Login</a>
                                    
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" aria-current="page" href="{{ url('/cv') }}">Curriculum Vitae</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        </nav>
      <div class="car">
          <div class="card-body">
              <table id="example" class="table table-bordered table-striped">
                    <thead>
                        <th>Kode Barang</th>
                        <th>No Kategori</th>
                        <th>Nama Barang</th>
                        <th>Harga Barang</th>
                        <th>Stok Ahir</th>
                    </thead>
                    <tbody>
                        @foreach($list_product as $list)
                            <tr>
                                <td>{{ $list->kd_barang }}</td>
                                <td>{{ $list->no_kategory }}</td>
                                <td>{{ $list->nama_barang }}</td>
                                <td>{{ $list->harga_barang }}</td>
                                <td>{{ $list->stock_ahir }}</td>
                            </tr>
                        @endforeach
                    </tbody>
              </table>
          </div>
      </div>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#example').DataTable();
            } );
        </script>
  </body>
</html>