<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>M Irfan Maulana</title>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
    <link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap" media="print" onload="this.media='all'"/>
    <noscript>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap"/>
    </noscript>
    <link href="{{ asset('css/font-awesome/css/all.min.css?ver=1.2.1') }}" rel="stylesheet">
    <link href="{{ asset('css/mdb.min.css?ver=1.2.1') }}" rel="stylesheet">
    <link href="{{ asset('css/aos.css?ver=1.2.1') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css?ver=1.2.1') }}" rel="stylesheet">
  </head>
  <body class="bg-light" id="top">
    <div class="page-content">
      <div class="container">
<div class="resume-container"><br><br><br>
  <div class="shadow-1-strong bg-white my-5" id="intro">
    <div class="bg-info text-white">
      <div class="cover bg-image"><img src="images/header-background.jpg"/>
        <div class="mask" style="background-color: rgba(0, 0, 0, 0.7);backdrop-filter: blur(2px);">
          <div class="text-center p-5"><br><br><br>
            <div class="avatar p-1"><img class="img-thumbnail shadow-2-strong" src="{{ asset('irfan.jpeg') }}" width="160" height="160"/></div>
            <div class="header-bio mt-3">
              <div data-aos="zoom-in" data-aos-delay="0">
                <h2 class="h1">M Irfan Maulana</h2>
                <p>Web Developer</p>
              </div>
              <!-- <div class="header-social mb-3 d-print-none" data-aos="zoom-in" data-aos-delay="200">
                <nav role="navigation">
                  <ul class="nav justify-content-center">
                    <li class="nav-item"><a class="nav-link" href="https://twitter.com/templateflip" title="Twitter"><i class="fab fa-twitter"></i><span class="menu-title sr-only">Twitter</span></a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="https://www.facebook.com/templateflip" title="Facebook"><i class="fab fa-facebook"></i><span class="menu-title sr-only">Facebook</span></a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="https://www.instagram.com/templateflip" title="Instagram"><i class="fab fa-instagram"></i><span class="menu-title sr-only">Instagram</span></a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="https://github.com/templateflip" title="Github"><i class="fab fa-github"></i><span class="menu-title sr-only">Github</span></a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div class="d-print-none"><a class="btn btn-outline-light btn-lg shadow-sm mt-1 me-3" href="material-resume.pdf" data-aos="fade-right" data-aos-delay="700">Download CV</a><a class="btn btn-info btn-lg shadow-sm mt-1" href="#contact" data-aos="fade-left" data-aos-delay="700">Hire Me</a></div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="shadow-1-strong bg-white my-5 p-5" id="about">
    <div class="about-section">
      <div class="row">
        <div class="col-md-6">
          <h2 class="h2 fw-light mb-4">Tentang Saya</h2>
          <p>Saya Muhamad Irfan M lulusan dari smk wikrama angkatan 2017-2018, saya
                mempunyai pengalaman kerja di Beberapa Perusahaan yg bergerak di bidang IT
                Konsultan dan saya mempunyai pengalaman menjadi vendor.Saya familiar
                dengan bahasa pemrograman php laravel,CI(codeigniter) dan yii2 dan familiar
                dengan RestApi.Untuk Bagian Frontend nya Yaitu Html5 / css dan Javascript.</p>
        </div>
        <div class="col-md-5 offset-lg-1">
          <div class="row mt-2">
            <h2 class="h2 fw-light mb-4">Bio</h2>
            <div class="col-sm-5">
              <div class="pb-2 fw-bolder"><i class="far fa-calendar-alt pe-2 text-muted" style="width:24px;opacity:0.85;"></i> Usia</div>
            </div>
            <div class="col-sm-7">
              <div class="pb-2">22</div>
            </div>
            <div class="col-sm-5">
              <div class="pb-2 fw-bolder"><i class="far fa-envelope pe-2 text-muted" style="width:24px;opacity:0.85;"></i> Email</div>
            </div>
            <div class="col-sm-7">
              <div class="pb-2">muhamadirfanm30@gmail.com</div>
            </div>
            <div class="col-sm-5">
              <div class="pb-2 fw-bolder"><i class="fas fa-phone pe-2 text-muted" style="width:24px;opacity:0.85;"></i> Nomor</div>
            </div>
            <div class="col-sm-7">
              <div class="pb-2">+6283863793845</div>
            </div>
            <div class="col-sm-5">
              <div class="pb-2 fw-bolder"><i class="fas fa-map-marker-alt pe-2 text-muted" style="width:24px;opacity:0.85;"></i> Alamat</div>
            </div>
            <div class="col-sm-7">
              <div class="pb-2">Jalan Letuan Suryanta No 99, Kabupaten Bogor</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="shadow-1-strong bg-white my-5 p-5" id="skills">
    <div class="skills-section">
      <h2 class="h2 fw-light mb-4">Kemampuan</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="mb-3"><span class="fw-bolder">PHP</span></div>
          <div class="mb-3"><span class="fw-bolder">LARAVEL</span></div>
          <div class="mb-3"><span class="fw-bolder">MySQL, POSTGREE</span></div>
          <div class="mb-3"><span class="fw-bolder">JavaScript</span></div>
        </div>
        <div class="col-md-6">
          <div class="mb-3"><span class="fw-bolder">MICROSOFT OFFICE</span></div>
          <div class="mb-3"><span class="fw-bolder">HTML</span></div>
          <div class="mb-3"><span class="fw-bolder">CSS</span></div>
          <div class="mb-3"><span class="fw-bolder">PHOTOSHOP</span></div>
        </div>
      </div>
    </div>
  </div>
  <div class="shadow-1-strong bg-white my-5 p-5" id="experience">
    <div class="work-experience-section">
      <h2 class="h2 fw-light mb-4">Pengalaman Kerja </h2>
      <div class="timeline">
        <div class="timeline-card timeline-card-info" data-aos="fade-in" data-aos-delay="200">
          <div class="timeline-head px-4 pt-3">
            <div class="h5">Web Developer <span class="text-muted h6">PT ASTech Solution</span></div>
          </div>
          <div class="timeline-body px-4 pb-4">
            <div class="text-muted text-small mb-3">2020 - 2021</div>
            <!-- <div>Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</div> -->
          </div>
        </div>
        <div class="timeline-card timeline-card-info" data-aos="fade-in" data-aos-delay="400">
          <div class="timeline-head px-4 pt-3">
            <div class="h5">Web Developer <span class="text-muted h6">PT PROGRESSTECH</span></div>
          </div>
          <div class="timeline-body px-4 pb-4">
            <div class="text-muted text-small mb-3">2018 - 2020</div>
            <!-- <div>User generated content in real-time will have multiple touchpoints for offshoring. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="shadow-1-strong bg-white my-5 p-5" id="education">
    <div class="education-section">
      <h2 class="h2 fw-light mb-4">Pendidikan</h2>
      <div class="timeline">
        <div class="timeline-card timeline-card-success" data-aos="fade-in" data-aos-delay="0">
          <div class="timeline-head px-4 pt-3">
            <div class="h5">Sekolah Menengah Kejuruan <span class="text-muted h6"> Wikrama Bogor</span>          </div>
          </div>
          <div class="timeline-body px-4 pb-4">
            <div class="text-muted text-small mb-3">2015 - 2018</div>
            <!-- <div>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</div> -->
          </div>
        </div>
        <div class="timeline-card timeline-card-success" data-aos="fade-in" data-aos-delay="200">
          <div class="timeline-head px-4 pt-3">
            <div class="h5">Madrasah Tsanawiah <span class="text-muted h6">Terpadu Yapisa</span>          </div>
          </div>
          <div class="timeline-body px-4 pb-4">
            <div class="text-muted text-small mb-3">2012 - 1015</div>
            <!-- <div>Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</div> -->
          </div>
        </div>
        <div class="timeline-card timeline-card-success" data-aos="fade-in" data-aos-delay="400">
          <div class="timeline-head px-4 pt-3">
            <div class="h5">Sekolah Dasar Negri <span class="text-muted h6">Sukamanah 01</span>          </div>
          </div>
          <div class="timeline-body px-4 pb-4">
            <div class="text-muted text-small mb-3">2006 - 2012</div>
            <!-- <div>User generated content in real-time will have multiple touchpoints for offshoring. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="shadow-1-strong bg-white my-5 p-5" id="education">
    <div class="education-section">
      <h2 class="h2 fw-light mb-4">Portofolio</h2>
      <div class="timeline">
        <div class="timeline-card timeline-card-success" data-aos="fade-in" data-aos-delay="0">
          <div class="timeline-head px-4 pt-3">
            <div class="h5">Camp Information System Aplication<span class="text-muted h6"> (CIS)</span>          </div>
          </div>
          <div class="timeline-body px-4 pb-4">
            <div>Sebuah System Management untuk mengelola data reservasi dan laporan di sebuah camping ground.</div>
          </div>
        </div>
        <div class="timeline-card timeline-card-success" data-aos="fade-in" data-aos-delay="200">
          <div class="timeline-head px-4 pt-3">
            <div class="h5">Multi Level Marketing Aplication<span class="text-muted h6">(MLM)</span>          </div>
          </div>
          <div class="timeline-body px-4 pb-4">
            <div>Aplikasi ini Mennyerupai sebuah ecomerce ada penjual dan pembeli yg akan menjadi member.</div>
          </div>
        </div>
        <div class="timeline-card timeline-card-success" data-aos="fade-in" data-aos-delay="400">
          <div class="timeline-head px-4 pt-3">
            <div class="h5">Astech Operational System <span class="text-muted h6">(AOS)</span>          </div>
          </div>
          <div class="timeline-body px-4 pb-4">
            <!-- <div class="text-muted text-small mb-3">2006 - 2012</div> -->
            <div>Sebuah Aplikasi Untuk Memudahkan customer berinteraksi dengan teknisi terkait kerusakan barang elektronik baik di rumah, apartemen, kantor dll.</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div></div>
    </div>
    <script src="{{ asset('scripts/mdb.min.js?ver=1.2.1') }}"></script>
    <script src="{{ asset('scripts/aos.js?ver=1.2.1') }}"></script>
    <script src="{{ asset('scripts/main.js?ver=1.2.1') }}"></script>
  </body>
</html>